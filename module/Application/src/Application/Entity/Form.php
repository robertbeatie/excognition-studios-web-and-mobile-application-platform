<?php

/* 
 * Excognition Studios
 * robertbeatie@xcogstudios.com
 */

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;

class Form{
    protected $fid;
    protected $fname;
    protected $fauthor;
    protected $fcontent;
}
