<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** @ORM\Entity 
 *  
 *  
 */
class Plant {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $pid;

    /** @ORM\Column(type="string") */
    protected $ptag;

    /** @ORM\Column(type="string") */
    protected $pname;


    // getters/setters
    /** @ORM\Column(type="text") */
    protected $pdiscription;
    
}
